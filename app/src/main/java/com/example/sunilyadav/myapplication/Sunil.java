package com.example.sunilyadav.myapplication;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by Sunil Yadav on 10/23/2017.
 */

public class Sunil extends Activity {

    Button btn;
    Button touch;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sunil);

        btn = (Button)findViewById(R.id.btn);


        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(Sunil.this, "welcome to sun",Toast.LENGTH_SHORT).show();
            }
        });


        touch = (Button)findViewById(R.id.touch);

        touch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(Sunil.this, "nice app",Toast.LENGTH_SHORT).show();
            }
        });


    }
}
